# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the plasma-pa package.
#
# Kishore G <kishore96@gmail.com>, 2021, 2022, 2023.
msgid ""
msgstr ""
"Project-Id-Version: plasma-pa\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-07 02:44+0000\n"
"PO-Revision-Date: 2023-05-14 15:17+0530\n"
"Last-Translator: Kishore G <kishore96@gmail.com>\n"
"Language-Team: Tamil <kde-i18n-doc@kde.org>\n"
"Language: ta\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 23.04.1\n"

#: contents/ui/DeviceListItem.qml:25
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/ui/DeviceListItem.qml:34
#, kde-format
msgid "Device name not found"
msgstr "சாதனத்தின் பெயர் தெரியாது"

#: contents/ui/ListItemBase.qml:68
#, kde-format
msgid "Currently not recording"
msgstr "இப்போது பதிவு செய்யவில்லை"

#: contents/ui/ListItemBase.qml:69
#, kde-format
msgid "Currently not playing"
msgstr "இப்போது இயங்கவில்லை"

#: contents/ui/ListItemBase.qml:184
#, kde-format
msgctxt "@action:button"
msgid "Additional Options"
msgstr "மேம்பட்ட அமைப்புகள்"

#: contents/ui/ListItemBase.qml:186
#, kde-format
msgid "Show additional options for %1"
msgstr "%1 என்பதற்கு கூடுதல் விருப்பங்களை காட்டு"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr "ஒலிமுடக்கத்தை நீக்கு"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Mute"
msgstr "ஒலியை அடக்கு"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Unmute %1"
msgstr "%1-இன் ஒலியை அனுமதி"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Mute %1"
msgstr "%1-இன் ஒலியை அடக்கு"

#: contents/ui/ListItemBase.qml:225
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "%1-க்கு ஒலியின் அளவை மாற்று"

#: contents/ui/ListItemBase.qml:284
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1%"

#: contents/ui/ListItemBase.qml:304
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: contents/ui/main.qml:37
#, kde-format
msgid "Audio Volume"
msgstr "ஒலி அளவு"

#: contents/ui/main.qml:44
#, kde-format
msgid "No output or input devices found"
msgstr "எந்த உள்ளீட்டு அல்லது வெளியீட்டு சாதனங்களும் கண்டுபிடிக்கப்படவில்லை"

#: contents/ui/main.qml:58
#, kde-format
msgid "Audio Muted"
msgstr "ஒலி அடக்கப்பட்டுள்ளது"

#: contents/ui/main.qml:60
#, kde-format
msgid "Volume at %1%"
msgstr "ஒலி %1% ஆக உள்ளது"

#: contents/ui/main.qml:78
#, kde-format
msgid "Middle-click to unmute"
msgstr "ஒலியை அனுமதிக்க நடு-கிளிக் செய்யவும்"

#: contents/ui/main.qml:79
#, kde-format
msgid "Middle-click to mute all audio"
msgstr "அனைத்து ஒலிகளையும் அடக்க நடு-கிளிக் செய்யவும்"

#: contents/ui/main.qml:80
#, kde-format
msgid "Scroll to adjust volume"
msgstr "ஒலி அளவை மாற்ற உருளவும்"

#: contents/ui/main.qml:244
#, kde-format
msgid "No output device"
msgstr "வெளியீட்டு சாதனம் இல்லை"

#: contents/ui/main.qml:255
#, kde-format
msgctxt "Device name (Battery percent)"
msgid "%1 (%2% Battery)"
msgstr "%1 (மின்கலம் %2%)"

#: contents/ui/main.qml:402
#, kde-format
msgid "Increase Volume"
msgstr "ஒலியை கூட்டு"

#: contents/ui/main.qml:408
#, kde-format
msgctxt "@action shortcut"
msgid "Increase Volume by 1%"
msgstr "ஒலியை 1% கூட்டு"

#: contents/ui/main.qml:414
#, kde-format
msgid "Decrease Volume"
msgstr "ஒலியை குறை"

#: contents/ui/main.qml:420
#, kde-format
msgctxt "@action shortcut"
msgid "Decrease Volume by 1%"
msgstr "ஒலியை 1% குறை"

#: contents/ui/main.qml:426
#, kde-format
msgid "Mute"
msgstr "ஒலியை அடக்கு"

#: contents/ui/main.qml:432
#, kde-format
msgid "Increase Microphone Volume"
msgstr "ஒலிவாங்கியின் ஒலியை கூட்டு"

#: contents/ui/main.qml:438
#, kde-format
msgid "Decrease Microphone Volume"
msgstr "ஒலிவாங்கியின் ஒலியை குறை"

#: contents/ui/main.qml:444
#, kde-format
msgid "Mute Microphone"
msgstr "ஒலிவாங்கியின் ஒலியை அடக்கு"

#: contents/ui/main.qml:546
#, kde-format
msgid "Devices"
msgstr "சாதனங்கள்"

#: contents/ui/main.qml:553
#, kde-format
msgid "Applications"
msgstr "செயலிகள்"

#: contents/ui/main.qml:574 contents/ui/main.qml:576 contents/ui/main.qml:790
#, kde-format
msgid "Force mute all playback devices"
msgstr "அனைத்து வெளியீட்டு சாதனங்களையும் கட்டாயமாக அடக்கு"

#: contents/ui/main.qml:629
#, kde-format
msgid "No applications playing or recording audio"
msgstr "எந்த செயலிகளும் ஒலியை இயக்கவோ பதிவு செய்யவோ இல்லை"

#: contents/ui/main.qml:781
#, kde-format
msgid "Raise maximum volume"
msgstr "அதிகபட்ச ஒலியை உயர்த்து"

#: contents/ui/main.qml:803
#, kde-format
msgid "Show virtual devices"
msgstr "மெய்நிகர் சாதனங்களைக் காட்டு"

#: contents/ui/main.qml:813
#, kde-format
msgid "&Configure Audio Devices…"
msgstr "ஒலி சாதனங்களை &அமை…"

#: contents/ui/StreamListItem.qml:25
#, kde-format
msgid "Stream name not found"
msgstr ""

#~ msgid "General"
#~ msgstr "பொது"

#~ msgid "Volume step:"
#~ msgstr "ஒலியின் அளவை மாற்றுவதற்கான படி:"

#~ msgid "Play audio feedback for changes to:"
#~ msgstr "இம்மாற்றங்களுக்கு ஒலியாக பின்னூட்டம் தா:"

#~ msgid "Audio volume"
#~ msgstr "ஒலியின் அளவு"

#~ msgid "Show visual feedback for changes to:"
#~ msgstr "இம்மாற்றங்களுக்கு காட்சிமுறையாக பின்னூட்டம் தா:"

#~ msgid "Microphone sensitivity"
#~ msgstr "ஒலிவாங்கியின் உணர்வுத்திறன்"

#~ msgid "Mute state"
#~ msgstr "ஒலியடக்க நிலை"

#~ msgid "Default output device"
#~ msgstr "இயல்பிருப்பு வெளியீடு சாதனம்"

#~ msgctxt "@title"
#~ msgid "Display:"
#~ msgstr "காட்டப்படுபவை:"

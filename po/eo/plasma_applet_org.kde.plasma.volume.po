# translation of plasma_applet_org.kde.plasma.volume.pot to Esperanto
# Copyright (C) 2021 Free Software Foundation, Inc.
# This file is distributed under the same license as the plasma-pa package.
# Oliver Kellogg <okellogg@users.sourceforge.net>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: plasma-pa\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-11-07 02:44+0000\n"
"PO-Revision-Date: 2023-11-12 16:39+0100\n"
"Last-Translator: Oliver Kellogg <okellogg@users.sourceforge.net>\n"
"Language-Team: Esperanto <kde-i18n-eo@kde.org>\n"
"Language: eo\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: translate-po (https://github.com/zcribe/translate-po)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: contents/ui/DeviceListItem.qml:25
#, kde-format
msgctxt "label of device items"
msgid "%1 (%2)"
msgstr "%1 (%2)"

#: contents/ui/DeviceListItem.qml:34
#, kde-format
msgid "Device name not found"
msgstr "Aparatonomo ne trovita"

#: contents/ui/ListItemBase.qml:68
#, kde-format
msgid "Currently not recording"
msgstr "Nuntempe ne registras"

#: contents/ui/ListItemBase.qml:69
#, kde-format
msgid "Currently not playing"
msgstr "Nuntempe ne ludas"

#: contents/ui/ListItemBase.qml:184
#, kde-format
msgctxt "@action:button"
msgid "Additional Options"
msgstr "Pliaj Opcioj"

#: contents/ui/ListItemBase.qml:186
#, kde-format
msgid "Show additional options for %1"
msgstr "Montri pliajn opciojn por %1"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Unmute"
msgstr "Malŝalti"

#: contents/ui/ListItemBase.qml:203
#, kde-format
msgctxt "@action:button"
msgid "Mute"
msgstr "Mute"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Unmute %1"
msgstr "Malŝalti %1"

#: contents/ui/ListItemBase.qml:205
#, kde-format
msgid "Mute %1"
msgstr "Muti %1"

#: contents/ui/ListItemBase.qml:225
#, kde-format
msgctxt "Accessibility data on volume slider"
msgid "Adjust volume for %1"
msgstr "Alĝustigi laŭtecon por %1"

#: contents/ui/ListItemBase.qml:284
#, kde-format
msgctxt "volume percentage"
msgid "%1%"
msgstr "%1 %"

#: contents/ui/ListItemBase.qml:304
#, kde-format
msgctxt "only used for sizing, should be widest possible string"
msgid "100%"
msgstr "100%"

#: contents/ui/main.qml:37
#, kde-format
msgid "Audio Volume"
msgstr "Audio Volumo"

#: contents/ui/main.qml:44
#, kde-format
msgid "No output or input devices found"
msgstr "Neniu eligo aŭ eniga aparatoj trovitaj"

#: contents/ui/main.qml:58
#, kde-format
msgid "Audio Muted"
msgstr "Aŭdio silentigita"

#: contents/ui/main.qml:60
#, kde-format
msgid "Volume at %1%"
msgstr "Volumo je %1%"

#: contents/ui/main.qml:78
#, kde-format
msgid "Middle-click to unmute"
msgstr "Mezklaku por malsilentigi"

#: contents/ui/main.qml:79
#, kde-format
msgid "Middle-click to mute all audio"
msgstr "Mezklaku por silentigi la tutan audion"

#: contents/ui/main.qml:80
#, kde-format
msgid "Scroll to adjust volume"
msgstr "Rulumu por ĝustigi laŭtecon"

#: contents/ui/main.qml:244
#, kde-format
msgid "No output device"
msgstr "Neniu eliga aparato"

#: contents/ui/main.qml:255
#, kde-format
msgctxt "Device name (Battery percent)"
msgid "%1 (%2% Battery)"
msgstr "%1 (%2% Baterio)"

#: contents/ui/main.qml:402
#, kde-format
msgid "Increase Volume"
msgstr "Pliigi Volumon"

#: contents/ui/main.qml:408
#, kde-format
msgctxt "@action shortcut"
msgid "Increase Volume by 1%"
msgstr "Pliigi Volumon je 1%"

#: contents/ui/main.qml:414
#, kde-format
msgid "Decrease Volume"
msgstr "Malpliigi laŭtecon"

#: contents/ui/main.qml:420
#, kde-format
msgctxt "@action shortcut"
msgid "Decrease Volume by 1%"
msgstr "Malpliigi laŭtecon je 1%"

#: contents/ui/main.qml:426
#, kde-format
msgid "Mute"
msgstr "Mute"

#: contents/ui/main.qml:432
#, kde-format
msgid "Increase Microphone Volume"
msgstr "Pliigi mikrofonan laŭtecon"

#: contents/ui/main.qml:438
#, kde-format
msgid "Decrease Microphone Volume"
msgstr "Malpliigi mikrofonan laŭteco"

#: contents/ui/main.qml:444
#, kde-format
msgid "Mute Microphone"
msgstr "Mute Mikrofono"

#: contents/ui/main.qml:546
#, kde-format
msgid "Devices"
msgstr "Aparatoj"

#: contents/ui/main.qml:553
#, kde-format
msgid "Applications"
msgstr "Aplikoj"

#: contents/ui/main.qml:574 contents/ui/main.qml:576 contents/ui/main.qml:790
#, kde-format
msgid "Force mute all playback devices"
msgstr "Devigi silentigi ĉiujn reproduktajn aparatojn"

#: contents/ui/main.qml:629
#, kde-format
msgid "No applications playing or recording audio"
msgstr "Neniuj aplikoj ludantaj aŭ registrante audio"

#: contents/ui/main.qml:781
#, kde-format
msgid "Raise maximum volume"
msgstr "Altigi maksimuman laŭtecon"

#: contents/ui/main.qml:803
#, kde-format
msgid "Show virtual devices"
msgstr "Montri virtualajn aparatojn"

#: contents/ui/main.qml:813
#, kde-format
msgid "&Configure Audio Devices…"
msgstr "&Agordi Aŭdio-Aparatojn…"

#: contents/ui/StreamListItem.qml:25
#, kde-format
msgid "Stream name not found"
msgstr "Fluonomo ne trovita"
